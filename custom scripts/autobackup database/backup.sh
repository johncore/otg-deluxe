#!/bin/bash

# ------------------------------------
# WARZONA AUTOBACKUP DATABASE
# ------------------------------------

#onde sera salvo o backup
CAMINHO="/home/otg-private/autobkpdatabase"

#nome que o script salvara o arquivo de backup .sql
NOMEBACKUP="database"

#usuário mysql - normalmente root
USER="usuario"

#senha do mysql - normalmente mesma do phpmyadmin
SENHA="senha"

#banco do servidor
BANCO="suadatabase"

# ------------------------------------
# Execução do script - nao mexer
# ------------------------------------
TEMPO="$(date +'%d-%m-%Y-%H-%M')"

if [[ -z "$USER" || -z "$SENHA" || -z "$BANCO" ]]; then
    echo "Por favor preencha o usuário, senha e banco de dados nas configurações."
else
    mysqldump -u$USER -p$SENHA $BANCO > $CAMINHO"/"$NOMEBACKUP"-"$TEMPO".sql" 
fi