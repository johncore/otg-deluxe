local config = {
    [30255] = Position(33384, 32627, 7),
    [30256] = Position(33314, 32647, 6)
}

function onUse(player, item, fromPosition, itemEx, toPosition)
	if player:getLevel() < 250 then
        player:sendCancelMessage('You do not feel experienced enough to traverse the dangerous surge of waves on this treacherous coast.')
		return true
	end	

    for actionId, destination in pairs(config) do
        if item.actionid == actionId then
            player:teleportTo(destination)
            destination:sendMagicEffect(CONST_ME_POFF)
            fromPosition:sendMagicEffect(CONST_ME_POFF)
            return true
        end
    end
end
