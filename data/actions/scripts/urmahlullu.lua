local config = {
	centerRoom = Position(33919, 31648, 8),
	BossPosition = Position(33919, 31642, 8),
	playerPositions = {
		Position(33918, 31626, 8), 
		Position(33919, 31626, 8),
		Position(33920, 31626, 8),
		Position(33921, 31626, 8),
		Position(33922, 31626, 8)
	},
	newPosition = Position(33918, 31657, 8)
}

function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if item.itemid == 9825 then
		if player:getPosition() ~= Position(33918, 31626, 8) then
			item:transform(9826)
			return true
		end
	end
	if item.itemid == 9825 then
		if player:getStorageValue(Storage.KillmareshQuest.Fafnar.UrmahlulluTimer) >= 1 then
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You need to wait a while, recently someone challenge Urmahlullu.")
			return true
		end
		local specs, spec = Game.getSpectators(config.centerRoom, false, false, 15, 15, 15, 15)
		for i = 1, #specs do
			spec = specs[i]
			if spec:isPlayer() then
				player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "Someone is fighting with Urmahlullu.")
				return true
			end
		end
		Game.createMonster("Urmahlullu the Weakened", config.BossPosition, true, true)
		for x = 33918, 33922 do
			local playerTile = Tile(Position(x, 31626, 8)):getTopCreature()
			if playerTile and playerTile:isPlayer() then
				playerTile:getPosition():sendMagicEffect(CONST_ME_POFF)
				playerTile:teleportTo(config.newPosition)
				playerTile:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
				playerTile:setExhaustion(Storage.KillmareshQuest.UrmahlulluTimer, 60 * 60 * 2 * 24)
			end
		end
		addEvent(clearForgotten, 30 * 60 * 1000, Position(33905, 31636, 8), Position(33934, 31666, 8), Position(33919, 31603, 8))
		item:transform(9826)
	elseif item.itemid == 9826 then
		item:transform(9825)
	end
	return true
end
