local talk = TalkAction("!bless")

function talk.onSay(player, words, param)
    local totalBlessPrice = getBlessingsCost(player:getLevel()) * 8 * 0.7
    if not player:isPzLocked() then
        if(player:hasBlessing(1) and player:hasBlessing(2) and player:hasBlessing(3) and player:hasBlessing(4) and player:hasBlessing(5) and player:hasBlessing(6) and player:hasBlessing(7) and player:hasBlessing(8)) then
            player:sendCancelMessage("You already have been blessed!", cid)
        elseif player:removeMoneyNpc(totalBlessPrice) then
            for b = 1, 8 do
                player:addBlessing(b, 1)
            end
            player:getPosition():sendMagicEffect(CONST_ME_HOLYAREA)
        else
            player:sendCancelMessage("You don't have enough money. You need " .. totalBlessPrice .. " to buy bless.", cid)
        end
    else
        player:sendCancelMessage("You can't buy bless while you are in a battle.")
    end
end

talk:separator(" ")
talk:register()