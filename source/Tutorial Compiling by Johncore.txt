# ------------------------------------
# Compilando em Debian ou Ubuntu
# ------------------------------------


#Install libs
apt -y install git cmake build-essential liblua5.2-dev libgmp3-dev libmysqlclient-dev libboost-system-dev libboost-iostreams-dev libboost-filesystem-dev libpugixml-dev libcrypto++-dev libspdlog-dev


#Generate the build files
cd /home/otg-private/source
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RelWithDebInfo ..
make clean -j 4
cmake ..

#Build
make -j`nproc`